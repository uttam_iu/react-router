
import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import Home from './components/Home';
import Contact from './components/Contact';
import About from './components/About';
import PropPass from './components/PropPass';
import Nav from './components/Nav';
import Post from './components/Post';
import Login from './components/Login';
import Auth from './Auth';
import Logout from './components/Logout';

class App extends Component {

	render() {
		console.log(Auth.isLogin)
		return (
			<BrowserRouter>
				<div>
					<Nav />
					<Switch>
						{/* normal component */}
						<Route path='/' exact component={Home} />
						<Route path='/contact' component={Contact} />
						<Route path='/about' component={About} />

						{/* if need to props passing to a component */}
						<Route path='/proppass' render={() => {
							return <PropPass name='uttam kumar' />
						}} />

						{/* dynamic routing */}
						{/* <Route path='/posts/:postId' component={Post} /> */}

						{/* dynamic routing with private*/}
						<PrivateRoute path='/posts/:postId' component={Post} />
						<Route path='/login' component={Login} />
						<Route path='/logout' component={Logout} />
					</Switch>
				</div>
			</BrowserRouter>
		);
	}
}

export default App;
