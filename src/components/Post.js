import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Post extends Component {
	render() {
		const { match } = this.props;
		return (
			<div style={{textAlign:'center'}}>
				<div>
					this post id is: {match.params.postId}
				</div>
				<Link to='/' >Back to Home</Link>
			</div>
		);
	}
}

export default Post;