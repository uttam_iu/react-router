import React, { Component } from 'react';
import { Redirect } from 'react-router';
import Auth from '../Auth';

class Logout extends Component {
	state = { redirect: false };

	componentDidMount() {
		Auth.logOut(() => {
			this.setState({ redirect: true });
		});
	}
	render() {
		if (this.state.redirect) return <Redirect to='/' />
		else return (<h1>Logging out .... </h1>);
	}
}

export default Logout;