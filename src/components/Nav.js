import React from 'react';
import { NavLink } from 'react-router-dom';
import Auth from '../Auth';

const Nav = () => {
	const navStyle = { padding: '8px', margin: '16px', background: '#f2f2f2', color: 'black' };
	const activeStyle = { background: 'green', color: '#fff' };
	console.log(Auth)
	return (
		<nav style={{ padding: '40px', textAlign: 'center' }}>
			<NavLink to='/' exact activeStyle={activeStyle} style={navStyle} >Home</NavLink>
			<NavLink to='/about' activeStyle={activeStyle} style={navStyle} >About</NavLink>
			<NavLink to='/contact' activeStyle={activeStyle} style={navStyle} >Contact</NavLink>
			<NavLink to='/proppass' activeStyle={activeStyle} style={navStyle}>PropPass</NavLink>
			{/* {Auth.isLogin &&  */}
			<NavLink to='/logout' activeStyle={activeStyle} style={navStyle} >Logout</NavLink>
			{/* // } */}
		</nav>
	);
}

export default Nav;