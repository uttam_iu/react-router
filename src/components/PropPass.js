import React, { Component } from 'react';

class PropPass extends Component {
    render() {
        return (
            <div style={{textAlign:'center'}}>
                {this.props.name} props is passed
            </div>
        );
    }
}

export default PropPass;