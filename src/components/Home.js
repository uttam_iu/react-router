import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
	render() {
		return (
			<div style={{textAlign:'center'}}>
				<div>home</div>
				<div style={{ display: 'flex', flexDirection: 'column' }}>
					<Link to='/posts/post-1' >post 1</Link>
					<Link to='/posts/post-3' >post 3</Link>
					<Link to='/posts/post-5' >post 5</Link>
					<Link to='/posts/post-6' >post 6</Link>
				</div>

			</div>
		);
	}
}

export default Home;