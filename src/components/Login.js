import React, { Component } from 'react';
import { Redirect } from 'react-router';
import Auth from '../Auth';

class Login extends Component {
	state = { redirect: false };

	login = () => {
		Auth.logIn(() => {
			this.setState({ redirect: true });
		});
	}

	render() {
		let { from } = this.props.location.state || { from: { pathname: '/' } }
		if (this.state.redirect) return <Redirect to={from.pathname} />

		return (
			<div style={{ textAlign: 'center' }}>
				<div>please login</div>
				<button onClick={this.login}>Log in</button>
			</div>
		);
	}
}

export default Login;